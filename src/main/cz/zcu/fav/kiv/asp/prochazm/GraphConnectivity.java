package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GraphConnectivity {
    public static void main(String[] args) throws IOException {
        final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int casesToGo = Integer.parseInt(br.readLine());
        br.readLine(); // read that ridiculous empty line...

        while (casesToGo-- > 0){
            int componentCount = br.readLine().charAt(0) - 'A' + 1;
            DisjointSet grapEqRelation = new DisjointSet(componentCount);
            String line;

            while ((line = br.readLine()) != null && !line.isEmpty()) {
                if (grapEqRelation.union(line.charAt(0) - 'A', line.charAt(1) - 'A')) {
                    --componentCount;
                }
            }

            System.out.print(componentCount + (casesToGo == 0 ? "\n" : "\n\n"));
        }
    }

    private static class DisjointSet {

        final int[] parents;

        DisjointSet(int size){
            this.parents = new int[size];
            for(int i = 0; i < size; ++i){
                this.parents[i] = i;
            }
        }

        private int find(int i){
            while (this.parents[i] != i){
                i = parents[i];
            }
            return i;
        }

        private boolean union(int i, int j){
            final int iRepresentative = find(i);
            final int jRepresentative = find(j);

            this.parents[iRepresentative] = jRepresentative;

            return iRepresentative != jRepresentative;
        }
    }
}
