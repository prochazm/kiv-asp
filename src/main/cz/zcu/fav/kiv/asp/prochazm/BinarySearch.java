package cz.zcu.fav.kiv.asp.prochazm;

import java.io.*;
import java.util.*;

class BinarySearch {
    // Binarni vyhledavani pomoci rozdel a panuj.
    // Pouzijte rekurzi, muzete pouzit List.subList(from, to).
    //
    // Vstup: Pole cisel usporadanych do neklesajici posloupnosti a dotazovane cislo x.
    // Vystup: true, pokud se x nachazi v poli, jinak false.
    static boolean binarySearch(List<Integer> a, Integer x) {
        int start = 0;
        int end = a.size() - 1;

        while (start != end - 1){
            int index = (start + end) / 2;
            int mid = a.get(index);
            if (x.equals(mid)){
                return true;
            } else if (mid < x){
                start = index;
            } else {
                end = index;
            }
        }

        return a.get(start + 1).equals(x);
    }

    public static void main(String[] args) throws IOException {
        // Rychlejsimu zpracovani vstupu a vystupu
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter pw = new PrintWriter(System.out);

        String line;
        while ((line = br.readLine()) != null) { // Cte radky az do EOF
            line = line.trim();
            if (line.isEmpty()) {
                break; // Ale na prazdnem radku taky skonci (uzivatelske testovani)
            }

            // Nacte serazene pole celych cisel
            List<Integer> a = new ArrayList<>();
            Integer prev = null;
            for (String x: line.split("\\s+")) {
                Integer curr = Integer.valueOf(x);
                a.add(curr);
                if (prev != null && prev > curr) {
                    throw new UnsupportedOperationException("Input must be sorted in non-descending order");
                }
                prev = curr;
            }
            a = Collections.unmodifiableList(a);

            // Nacte dotazy (queries)
            List<Integer> q = new ArrayList<>();
            for (String x: br.readLine().trim().split("\\s+")) {
                q.add(Integer.valueOf(x));
            }
            if (q.isEmpty()) {
                break;
            }

            // Najde cisla pomoci binarniho hledani
            StringBuilder sb = new StringBuilder();
            for (Integer x: q) {
                sb.append(binarySearch(a, x)).append(' ');
            }

            // Vypise radek usporadanych cisel
            pw.println(sb.toString().trim());

            pw.flush(); // jen kvuli uzivatelskemu testovani
        }

        pw.close(); // Musime zavrit vystup, jinak nemusi byt kompletni!
    }
}