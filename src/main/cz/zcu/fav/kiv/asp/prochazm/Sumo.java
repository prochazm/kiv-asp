package cz.zcu.fav.kiv.asp.prochazm;

import java.io.*;

public class Sumo {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int count = Integer.parseInt(br.readLine());
        long sum = 0;

        for (int i = 0; i < count; ++i){
            sum += Integer.parseInt(br.readLine());
        }

        System.out.println(sum);
    }
}