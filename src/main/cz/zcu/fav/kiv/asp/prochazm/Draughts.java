package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Draughts {
    public static void main(String[] args) throws IOException {
        final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        final int count = Integer.parseInt(br.readLine());

        for (int i = 0; i < count; ++i){
            br.readLine();

            Field[][] state = new Field[10][10];

            for (int j = 0; j < 10; ++j){
                char[] line = br.readLine().toCharArray();
                for (int k = 0; k < 10; ++k) {
                    state[j][k] = Field.valueOf(line[k]);
                }
            }

            int result = 0;
            for (int j = 0; j < 10; ++j) {
                for (int k = 0; k < 10; ++k) {
                    if (state[j][k] == Field.WHITE){
                        result = Integer.max(solve(state, j, k, 0), result);
                    }
                }
            }

            System.out.println(result);
        }
    }

    private static int solve(Field[][] state, int row, int col, int result){
        int newResult = result;

        for (Direction direction : Direction.values()) {
            if (direction.isValidMove(state, row, col)){
                direction.apply(state, row, col);
                newResult = Integer.max(
                        solve(state, direction.getNewRow(row), direction.getNewCol(col), result + 1),
                        newResult
                );
                direction.revert(state, row, col);
            }
        }

        return newResult;
    }

    private enum Field{
        EMPTY,
        WHITE,
        BLACK;

        public static Field valueOf(char c){
            switch (c){
                case '.':
                case '#': return EMPTY;
                case 'B': return BLACK;
                case 'W': return WHITE;
                default : return EMPTY;
            }
        }
    }

    private enum Direction{
        UPLEFT(-1, -1),
        UPRIGHT(-1, 1),
        DOWNLEFT(1, -1),
        DOWNRIGHT(1, 1);

        final int rowOffset;
        final int colOffset;
        final int rowOffsetDouble;
        final int colOffsetDouble;

        Direction(int rowOffset, int colOffset){
            this.rowOffset = rowOffset;
            this.colOffset = colOffset;
            this.rowOffsetDouble = rowOffset * 2;
            this.colOffsetDouble = colOffset * 2;
        }

        public int getNewRow(int row){
            return row + rowOffsetDouble;
        }

        public int getNewCol(int col){
            return col + colOffsetDouble;
        }

        public void apply(Field[][] state, int row, int col){
            state[row][col] = Field.EMPTY;
            state[row + rowOffset][col + colOffset] = Field.EMPTY;
            state[row + rowOffsetDouble][col + colOffsetDouble] = Field.WHITE;
        }

        public void revert(Field[][] state, int row, int col){
            state[row][col] = Field.WHITE;
            state[row + rowOffset][col + colOffset] = Field.BLACK;
            state[row + rowOffsetDouble][col + colOffsetDouble] = Field.EMPTY;
        }

        public boolean isValidMove(Field[][] state, int row, int col) {
            final int newRow = getNewRow(row);
            final int newCol = getNewCol(col);

            return newRow >= 0 && newRow <= 9 && newCol >= 0 && newCol <= 9
                    && state[row + rowOffset][col + colOffset] == Field.BLACK
                    && state[row + rowOffsetDouble][col + colOffsetDouble] == Field.EMPTY;
        }
    }
}
