package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Kasino {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Map<String, Integer> playerScore = new HashMap<>();
        String line;

        while((line = br.readLine()) != null){
            if (line.trim().length() == 0){
                // input is guaranteed to be valid so this ugly get should not be problem...
                System.out.println(playerScore.values().stream().max(Integer::compare).get());
                playerScore.clear();
                continue;
            }
            String[] lineSplit = line.split("\\s+");
            Integer entry = playerScore.get(lineSplit[0]);

            playerScore.put(lineSplit[0], entry == null
                    ? Integer.parseInt(lineSplit[1])
                    : entry + Integer.parseInt(lineSplit[1])
            );
        }
        // neither this one
        System.out.print(playerScore.values().stream().max(Integer::compare).get());
    }
}
