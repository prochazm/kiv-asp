package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class KeyTask {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String[] line;

        while (!(line = br.readLine().split(" "))[0].equals("0")){
            final int height = Integer.parseInt(line[0]);
            final int width = Integer.parseInt(line[1]);

            final char[][] map = new char[height][];
            Coordinate start = null;

            // fill in map and find start
            for (int i = 0; i < height; ++i){
                map[i] = br.readLine().toCharArray();
                if (start != null) {
                    continue;
                }
                for (int j = 0; j < width; j++) {
                    if (map[i][j] == '*'){
                        start = new Coordinate(i, j, 0);
                        break;
                    }
                }
            }

            boolean[][][] visited = new boolean[height][width][5];
            Deque<Coordinate> que = new ArrayDeque<>();
            int distance = 0;

            que.offer(start);

            bfs:
            while (!que.isEmpty()){
                Coordinate c = que.poll();
                List<Coordinate> neigbours = c.getNeighbours();

                for (int i = 0b0000, j = 0; i <= 0b1000; i = i == 0 ? 1 : i << 1, ++j) {

                    if (i != 0 && (i & c.keys) == 0){
                        continue;
                    }

                    for (Coordinate neighbour : neigbours) {
                        if (neighbour.x < 0 || neighbour.x >= height || neighbour.y < 0 || neighbour.y >= width) {
                            continue;
                        } else if (visited[neighbour.x][neighbour.y][j]){
                            continue;
                        }

                        char cValue = map[neighbour.x][neighbour.y];
                        if (cValue == '#'){
                            continue;
                        } else if (cValue == 'X'){
                            distance = neighbour.distance;
                            break bfs;
                        }

                        int key = keyToIndex(cValue);
                        if (key > 0){
                            neighbour.setKeys(c.keys | key);
                        } else {
                            neighbour.setKeys(c.keys);
                            int door = doorToIndex(cValue);
                            if (door > 0 && (neighbour.keys & door) == 0){
                                continue;
                            }
                        }

                        que.offer(neighbour);
                        visited[neighbour.x][neighbour.y][j] = true;
                    }
                }
            }

            if (distance == 0) {
                System.out.println("The poor student is trapped!");
            } else {
                System.out.println(String.format("Escape possible in %d steps.", distance));
            }
            br.readLine();
        }

    }

    private static int keyToIndex(char key){
        switch (key){
            case 'b': return 0b0001;
            case 'y': return 0b0010;
            case 'r': return 0b0100;
            case 'g': return 0b1000;
            default : return 0b0000;
        }
    }

    private static int doorToIndex(char door){
        return keyToIndex(Character.toLowerCase(door));
    }

    private static class Coordinate{
        final int x;
        final int y;
        int keys;
        int distance;

        private Coordinate(int x, int y, int distance) {
            this.x = x;
            this.y = y;
            this.distance = distance;
        }

        void setKeys(int keys){
            this.keys = keys;
        }

        List<Coordinate> getNeighbours(){
            List<Coordinate> coordinates = new ArrayList<>();
            int d = distance + 1;

            coordinates.add(new Coordinate(x + 1, y, d));
            coordinates.add(new Coordinate(x, y + 1, d));
            coordinates.add(new Coordinate(x, y - 1, d));
            coordinates.add(new Coordinate(x - 1, y, d));

            return coordinates;
        }
    }
}
