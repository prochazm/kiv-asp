package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class CountingStars {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while (!(line = br.readLine()).equals("0 0")) {

            String[] l = line.split(" ");
            int height = Integer.parseInt(l[0]);
            int width = Integer.parseInt(l[1]);

            char[][] map = new char[height + 2][width + 2];
            Arrays.fill(map[0], '.');
            Arrays.fill(map[height + 1], '.');

            for (int i = 1; i <= height; ++i) {
                char[] lineAsArray = br.readLine().toCharArray();
                map[i][0] = '.';
                map[i][map[i].length - 1] = '.';
                System.arraycopy(lineAsArray, 0, map[i], 1, width);
            }

            int counter = 0;

            for (int i = 1; i < map.length - 1; ++i) {
                for (int j = 1; j < map[i].length - 1; ++j) {
                    if (map[i][j] == '*') {
                        if (map[i][j - 1] == '.' && map[i][j + 1] == '.'
                                && map[i - 1][j] == '.' && map[i + 1][j] == '.'
                                && map[i - 1][j - 1] == '.' && map[i + 1][j + 1] == '.'
                                && map[i - 1][j + 1] == '.' && map[i + 1][j - 1] == '.') {
                            ++counter;
                        }
                    }
                }
            }

            System.out.println(counter);
        }
    }
}
