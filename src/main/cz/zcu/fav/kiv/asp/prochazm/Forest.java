package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Forest {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        final int caseCount = Integer.parseInt(br.readLine());

        for (int i = 0; i < caseCount; ++i){
            int vertices = 0x00;
            int edgeCount = 0;
            String line;

            while((line = br.readLine()).charAt(0) != '*'){
                vertices |= 0x01 << line.charAt(1) - 'A' | 0x01 << line.charAt(3) - 'A';

                ++edgeCount;
            }

            String[] allVertices = br.readLine().split(",");
            int acornCount = 0;
            for (String v : allVertices) {
                if ((vertices >>> v.charAt(0) - 'A' & 0x01) != 0x01){
                    ++acornCount;
                }
            }

            final int treeCount = allVertices.length - edgeCount - acornCount;

            System.out.println(String.format("There are %d tree(s) and %d acorn(s).", treeCount, acornCount));
        }
    }
}
