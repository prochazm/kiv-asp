package cz.zcu.fav.kiv.asp.prochazm;

import java.io.*;
import static java.lang.Integer.*;

public class Bishop {

    public static void main(String[] args)  throws IOException {
        final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        final int[] diagonals = new int[20000000];
        int max = 0;

        for (int i = parseInt(br.readLine()); i > 0; --i) {
            String[] f = br.readLine().split(" ");
            final int x = parseInt(f[0]);
            final int y = parseInt(f[1]);

            max = Integer.max(max, Integer.max(
                ++diagonals[x + y],
                ++diagonals[y - x + 15000001]
            ));
        }

        System.out.println(max);
    }
}