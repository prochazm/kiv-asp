package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;

public class CountingRooms {

    static class Coordinate {
        final int x;
        final int y;

        Coordinate(int x, int y){
            this.x = x;
            this.y = y;
        }

        private Coordinate getLeft(){
            return (this.x == 0) ? null : new Coordinate(this.x - 1, this.y);
        }

        private Coordinate getRight(int maxX){
            return (this.x == maxX) ? null : new Coordinate(this.x + 1, this.y);
        }

        private Coordinate getTop(){
            return (this.y == 0) ? null : new Coordinate(this.x, this.y - 1);
        }

        private Coordinate getBottom(int maxY){
            return (this.y == maxY) ? null : new Coordinate(this.x, this.y +  1);
        }

        Set<Coordinate> getNeightbours(int maxX, int maxY) {
            Set<Coordinate> neighbours = new HashSet<>();
            neighbours.add(getLeft());
            neighbours.add(getTop());
            neighbours.add(getBottom(maxY));
            neighbours.add(getRight(maxX));

            return neighbours;
        }
    }

    static int countRooms(char[][] a) {
        int roomCount = 0;
        final int maxX = a.length - 1;
        final int maxY = a[0].length - 1;

        for (int x = 0; x <= maxX; ++x) {
            for (int y = 0; y <= maxY; ++y) {
                if (a[x][y] != '.') {
                    continue;
                }

                Deque<Coordinate> que = new ArrayDeque<>();
                que.offer(new Coordinate(x, y));

                while (!que.isEmpty()){
                    Coordinate c = que.poll();
                    for (Coordinate n : c.getNeightbours(maxX, maxY)) {
                        if (n != null && a[n.x][n.y] == '.'){
                            a[n.x][n.y] = '#';
                            que.offer(n);
                        }
                    }
                }
                ++roomCount;
            }
        }

        return roomCount;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        char[][] a = new char[n][];
        for (int i = 0; i < n; ++i) {
            a[i] = br.readLine().toCharArray();
        }
        System.out.println(countRooms(a));
    }
}
