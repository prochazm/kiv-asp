package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;

public class Jezdec {
        public static void main(String[] args) throws IOException {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            int n;
            while ((n = Integer.parseInt(br.readLine())) != 0) {
                char[][] board = new char[n][];
                for (int i = 0; i < n; ++i) {
                    board[i] = br.readLine().toCharArray();
                }
                System.out.println(pathCost(board));
            }
        }

        private static Coordinate findStart(char[][] board){
            for (int i = 0; i < board.length; ++i){
                for (int j = 0; j < board.length; ++j){
                    if (board[i][j] == 'S'){
                        return new Coordinate(i, j, 0, board.length);
                    }
                }
            }
            return null;
        }

        private static int pathCost(char[][] board) {
            Deque<Coordinate> coordinates = new ArrayDeque<>();
            coordinates.offer(findStart(board));

            while(!coordinates.isEmpty()){
                Coordinate coordinate = coordinates.poll();
                for (Coordinate c : coordinate.getNextStates()) {
                    switch(board[c.x][c.y]){
                        case 'C': return c.distance;
                        case '.': board[c.x][c.y] = 'X';
                                  coordinates.offer(c);
                    }
                }
            }

            return -1;
        }

        private static class Coordinate{
            final static int[][] offsets = new int[][]{
                    {-2, -1},{-2,  1},
                    {-1, -2},{ 1, -2},
                    { 2, -1},{ 2,  1},
                    {-1,  2},{ 1,  2}
            };

            final int x;
            final int y;
            final int distance;
            final int boardSize;

            Coordinate(final int x, final int y, final int distance, final int boardSize){
                this.x = x;
                this.y = y;
                this.distance = distance;
                this.boardSize = boardSize;
            }

            Set<Coordinate> getNextStates() {
                Set<Coordinate> possibleJumps = new HashSet<>(8);

                for (int[] offset : offsets) {
                    int newX = this.x + offset[0];
                    int newY = this.y + offset[1];

                    if (Integer.min(newX, newY) < 0 || Integer.max(newX, newY) >= this.boardSize){
                        continue;
                    }

                    possibleJumps.add(new Coordinate(newX, newY, this.distance + 1, this.boardSize));
                }

                return possibleJumps;
            }
        }
}
