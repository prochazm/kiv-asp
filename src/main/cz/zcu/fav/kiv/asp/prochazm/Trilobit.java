package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Trilobit {

    private static long[] trilobits = new long[] {
        0b000, 0b100, 0b010, 0b110,
        0b001, 0b101, 0b011, 0b111,
    };

    public static void main(String[] args) throws IOException {
        final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String line;
        while ((line = br.readLine()) != null){
            long pattern = Long.parseLong(line);
            final long trilobit = pattern & 0x07;
            final long revTrilobit = trilobits[(int)trilobit];

            int count = 0;
            for (int i = 0; i <= 29; ++i){
                if ((pattern ^ trilobit) << 61 == 0
                    || (pattern ^ revTrilobit) << 61 == 0){
                    ++count;
                }
                pattern >>>= 1;
            }

            System.out.println(count);
        }
    }
}
