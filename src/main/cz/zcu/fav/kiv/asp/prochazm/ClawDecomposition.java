package cz.zcu.fav.kiv.asp.prochazm;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

import static java.lang.Integer.*;

public class ClawDecomposition {

    public static void main(String... args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int vertexCount;
        while ((vertexCount = parseInt(br.readLine())) != 0) {
            List<Node> nodes = new ArrayList<>();
            for (int i = 0; i < vertexCount; i++) {
                nodes.add(new Node());
            }

            String[] s = br.readLine().split(" ");
            for (; !s[0].equals("0"); s = br.readLine().split(" ")){
                nodes.get(parseInt(s[0]) - 1).connect(nodes.get(parseInt(s[1]) - 1));
            }

            System.out.println(nodes.get(0).bfs() ? "YES" : "NO");
        }
    }

    static class Node{
        private final Node[] neighbours = new Node[3];
        private int index = 0;
        private Boolean color = null;

        void connect(Node i){
            neighbours[index++] = i;
            i.neighbours[i.index++] = this;
        }

        boolean bfs(){
            Deque<Node> que = new ArrayDeque<>();
            que.offer(this);
            this.color = true;

            while (!que.isEmpty()){
                Node n = que.poll();
                for (Node neighbour : n.neighbours) {
                    if (neighbour.color == null){
                        neighbour.color = !n.color;
                        que.offer(neighbour);
                    } else if (neighbour.color == n.color){
                        return false;
                    }
                }
            }
            return true;
        }
    }
}