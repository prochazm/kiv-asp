package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class TeamQueue {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int scenario = 0;
        int teamCount;

        while((teamCount = Integer.parseInt(br.readLine())) != 0){

            System.out.println("Scenario #" + ++scenario);

            Map<Integer, Integer> teamMemberMapping = new HashMap<>(100);

            for (int i = 0; i < teamCount; ++i){
                int[] line = Arrays.stream(br.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();
                for (int j = 1; j < line.length; ++j){
                    teamMemberMapping.put(line[j], i);
                }
            }

            String line;
            LinkedHashMap<Integer, Deque<Integer>> teamQue = new LinkedHashMap<>(100);

            while (!(line = br.readLine()).equals("STOP")){
                String[] contents = line.split(" ");

                if (contents.length == 2){
                    // ENQUE
                    int memberId = Integer.parseInt(contents[1]);
                    Integer team = teamMemberMapping.get(memberId);
                    Deque<Integer> memberQue = teamQue.get(team);

                    if (memberQue == null){
                        memberQue = new ArrayDeque<>();
                        teamQue.put(team, memberQue);
                    }
                    memberQue.offer(memberId);
                } else {
                    // DEQUEUE
                    Map.Entry<Integer, Deque<Integer>> firstEntry = teamQue.entrySet().iterator().next();
                    Deque<Integer> members = firstEntry.getValue();

                    System.out.println(members.poll());

                    if (members.isEmpty()){
                        teamQue.remove(firstEntry.getKey());
                    }
                }
            }

            System.out.println();
        }
    }
}
