package cz.zcu.fav.kiv.asp.prochazm;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Rails {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        while (Integer.parseInt(br.readLine()) != 0) {
            for (int[] template = grabInts(br); template[0] != 0; template = grabInts(br)){

                IntStack station = new IntStack(template.length);
                int index = 0;

                for(int current = 1; current <= template.length; ++current){
                    station.push(current);

                    while(!station.isEmpty() && station.peek() == template[index] && index++ < template.length){
                        station.pop();
                    }
                }

                System.out.println(station.isEmpty() ? "Yes" : "No");
            }
            System.out.println();
        }
    }

    private static int[] grabInts(BufferedReader reader) throws IOException {
        return Arrays.stream(reader.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();
    }

    private static class IntStack {
        int[] items;
        int index;

        IntStack(int size){
            this.items = new int[size];
            this.index = 0;
        }

        void push(int i){
           this.items[this.index++] = i;
        }

        void pop(){
            --this.index;
        }

        int peek(){
            return this.items[this.index - 1];
        }

        boolean isEmpty(){
            return index == 0;
        }
    }
}


