package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class WayOut {

    // a - bludiste bez vyznacene startovni pozice
    // i, j - startovni pozice (volne policko)
    // Vraci true, pokud se lze dostat ze startu do cile, jinak false
    private static boolean solve(char[][] a, int i, int j) {
        if (a[i][j] == 'x'){
            return true;
        } else {
            a[i][j] = '#';
        }

        boolean result = false;

        if (i < a.length - 1 && a[i + 1][j] != '#') {
            result = solve(a, i + 1, j);
        } if (i > 0 && a[i - 1][j] != '#') {
            result |= solve(a, i - 1, j);
        } if (j < a[0].length - 1 && a[i][j + 1] != '#'){
            result |= solve(a, i, j + 1);
        } if (j > 0 && a[i][j - 1] != '#'){
            result |= solve(a, i, j - 1);
        }

        return result;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());

        int si = -1, sj = -1;
        char[][] a = new char[n][];
        for (int i = 0; i < n; ++i) {
            String line = br.readLine();
            a[i] = line.toCharArray();
            sj = sj != -1 ? sj : line.indexOf('*');
            si = si != -1 ? si : i;
        }
        a[si][sj] = '.';
        System.out.println(solve(a, si, sj));
    }

}
