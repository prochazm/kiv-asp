package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

import static java.lang.Integer.parseInt;

public class Fire {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        for (int casesToGo = parseInt(br.readLine()); casesToGo > 0; --casesToGo){
            char[][] map = new char[parseInt(br.readLine().split(" ")[0])][];
            List<Triple> fires = new ArrayList<>();
            Triple joe = null;

            for (int i = 0; i < map.length; i++) {
                map[i] = br.readLine().toCharArray();
                for (int j = 0; j < map[i].length; j++) {
                    switch (map[i][j]) {
                        case 'F': fires.add(new Triple(0, i, j)); continue;
                        case 'J': joe = new Triple(0, i, j);
                    }
                }
            }

            assert Objects.nonNull(joe);

            Deque<Triple> que = new ArrayDeque<>();
            que.offer(joe);
            fires.forEach(que::offer);

            int result = 0;
            int[] x = new int[4];
            int[] y = new int[4];

            outer:
            while (!que.isEmpty()){
                Triple p = que.poll();
                x[0] = p.x + 1;     y[0] = y[1] = p.y;
                x[1] = p.x - 1;     y[2] = p.y + 1;
                x[2] = x[3] = p.x;  y[3] = p.y - 1;

                for (int i = 0; i < 4; i++) {
                    if (x[i] < map.length && y[i] < map[0].length && x[i] >= 0 && y[i] >= 0){
                        if (map[x[i]][y[i]] != '#' && map[x[i]][y[i]] != 'F') {
                            if (map[x[i]][y[i]] == 'J' && map[p.x][p.y] == 'J'){
                                continue;
                            }
                            map[x[i]][y[i]] = map[p.x][p.y];
                            que.offer(new Triple(p.dist + 1, x[i], y[i]));
                        }
                    } else if (map[p.x][p.y] == 'J') {
                        result = p.dist + 1;
                        break outer;
                    }
                }
            }

            System.out.println(result > 0 ? result : "IMPOSSIBLE");
        }
    }

    private static class Triple{
        final int x;
        final int y;
        final int dist;

        Triple(int dist, int x, int y){
            this.x = x;
            this.y = y;
            this.dist = dist;
        }
    }
}
