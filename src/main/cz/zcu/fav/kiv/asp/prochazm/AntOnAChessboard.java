package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static java.lang.Integer.parseInt;

public class AntOnAChessboard {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        for (int i = parseInt(br.readLine()); i != 0; i = parseInt(br.readLine())){
            int p = (int)Math.sqrt((double)i);
            int diff = i - p*p;

            int c1, c2;

            if (p % 2 == 0){
                c1 = p;
                c2 = 1;
                if (i != p * p) {
                    --diff;
                    c2 += Integer.min(diff, c1);
                    diff -= c1++;
                    if (diff > 0) {
                        c1 -= diff;
                    }
                }
            } else {
                c1 = 1;
                c2 = p;
                if (i != p * p) {
                    --diff;
                    c1 += Integer.min(diff, c2);
                    diff -= c2++;
                    if (diff > 0){
                        c2 -= diff;
                    }
                }
            }

            System.out.println(String.format("%d %d", c1, c2));
        }
    }

}
