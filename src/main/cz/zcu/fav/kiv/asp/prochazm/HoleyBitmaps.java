package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HoleyBitmaps {

    public static void main(String[] args) throws IOException {
        final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        boolean done = false;

        while (!done) {
            final int[] bitmap = new int[32];
            String line;
            int index = 0;

            while (!(done = (line = br.readLine()) == null) && !line.equals("")) {
                bitmap[index++] = Integer.parseUnsignedInt(line.substring(2), 16);
            }

            int count = 0;
            for (int i = 0; i < 32; ++i){
                if (bitmap[i] != 0){
                    for (int j = 0; j < 32; ++j){
                        if ((bitmap[i] >>> j & 0b0001) != 0){
                            dfs(i, j, bitmap);
                            ++count;
                            ++j;
                        }
                    }
                }
            }

            System.out.println(count);
        }
    }

    private static void dfs(int index, int shift, int[] bitmap){
        if (index < 0 || index >= 32){
            return;
        } else if (shift < 0 || shift >= 32){
            return;
        }

        if ((bitmap[index] >>> shift & 0b0001) != 0) {
            bitmap[index] &= ~(0b0001 << shift);

            dfs(index, shift + 1, bitmap);
            dfs(index, shift - 1, bitmap);
            dfs(index - 1, shift + 1, bitmap);
            dfs(index - 1, shift, bitmap);
            dfs(index - 1, shift - 1, bitmap);
            dfs(index + 1, shift + 1, bitmap);
            dfs(index + 1, shift, bitmap);
            dfs(index + 1, shift - 1, bitmap);
        }
    }
}
