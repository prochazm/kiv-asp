package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;

public class Passwords {
    public static void main(String[] args) throws IOException {
        final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line;

        while ((line = br.readLine()) != null) {
            final String[] words = new String[Integer.parseInt(line)];

            for (int i = 0; i < words.length; ++i) {
                words[i] = br.readLine();
            }

            final String[] patterns = new String[Integer.parseInt(br.readLine())];

            for (int i = 0; i < patterns.length; ++i) {
                patterns[i] = br.readLine();
            }

            System.out.println("--");
            for (String pattern : patterns) {
                generate(pattern, words, 0, new Stack<>());
            }
        }
    }

    private static void generate(String pattern, String[] words, int index, Stack<String> result){
        if (index == pattern.length()){
            System.out.println(toString(result));
            return;
        }

        if (pattern.charAt(index) == '#'){
            for (String word : words) {
                result.push(word);
                generate(pattern, words, index + 1, result);
                result.pop();
            }
        } else {
            for (int i = 0; i < 10; ++i){
                result.push(String.valueOf(i));
                generate(pattern, words, index + 1, result);
                result.pop();
            }
        }
    }

    private static String toString(Stack<String> deque){
        StringBuilder sb = new StringBuilder();
        for (String s : deque) {
            sb.append(s);
        }
        return sb.toString();
    }
}
