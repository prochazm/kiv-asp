package cz.zcu.fav.kiv.asp.prochazm;

import java.io.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

class QuickSort {

    // Quicksort pomoci rozdel a panuj.
    //
    // Vstup: Neusporadane pole cisel
    // Vystup: Pole cisel usporadane do neklesajici posloupnosti
    //
    // Pouzijte rekurzi a pomocna pole (List/ArrayList)
    static List<Integer> quicksort(List<Integer> a) {
        int[] result = a.stream().mapToInt(Integer::intValue).toArray();

        reorder(result, 0, a.size() - 1);

        return Arrays.stream(result).boxed().collect(Collectors.toList());
    }

    static void reorder(int[] a, int from, int to){
        if (to - from == 0 || from > to){
            return;
        }

        int p = a[ThreadLocalRandom.current().nextInt(from, to)];
        int l = from;
        int r = to;

        while (l <= r){
            for (; a[l] < p; ++l);
            for (; a[r] > p; --r);
            if (l <= r){
                int tmp = a[l];
                a[l++] = a[r];
                a[r--] = tmp;
            }
        }

        reorder(a, from, r);
        reorder(a, l, to);
    }

    public static void main(String[] args) throws IOException {
        // Rychlejsimu zpracovani vstupu a vystupu
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter pw = new PrintWriter(System.out);

        String line;
        while ((line = br.readLine()) != null) { // Cte radky az do EOF

            // Nacte pole celych cisel
            List<Integer> a = new ArrayList<>();
            for (String x: line.trim().split("\\s+")) {
                a.add(Integer.valueOf(x));
            }

            // Prazdny radek => konec (jen kvuli uzivatelskemu testovani)
            if (a.isEmpty()) {
                break;
            }

            // Usporada cisla vzestupne
            Iterator<Integer> it = quicksort(Collections.unmodifiableList(a)).iterator();

            // Na vystup pujdou usporadana cisla oddelena mezerami
            StringBuilder sb = new StringBuilder();
            sb.append(it.next());
            while (it.hasNext()) {
                sb.append(' ').append(it.next());
            }

            // Vypise radek usporadanych cisel
            pw.println(sb);

            pw.flush(); // jen kvuli uzivatelskemu testovani
        }

        pw.close(); // Musime zavrit vystup, jinak nemusi byt kompletni!
    }
}