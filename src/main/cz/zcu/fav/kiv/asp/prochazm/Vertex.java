package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

import static java.lang.Integer.parseInt;

public class Vertex {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line;
        List<Integer> tmp;

        while (!(line = br.readLine()).equals("0")) {
            int vertexCount = parseInt(line);
            List<List<Integer>> adjacencyList = new ArrayList<>(vertexCount);

            for (int i = 0; i < vertexCount; ++i){
                adjacencyList.add(new ArrayList<>());
            }

            while ((line = br.readLine()).length() != 1) {
                String[] vertices = line.split(" ");
                tmp = adjacencyList.get(parseInt(vertices[0]) - 1);

                for (int i = 1; i < vertices.length - 1; ++i){
                    tmp.add(parseInt(vertices[i]));
                }
            }

            line = br.readLine();
            String[] splitLine = line.split(" ");
            for (int i = 1; i < splitLine.length; ++i) {

                StringBuilder sb = new StringBuilder();
                int vertex = 0;
                int unvisitedCount = 0;

                for (boolean b: dfs(adjacencyList, parseInt(splitLine[i]))) {
                    ++vertex;
                    if (!b){
                        sb.append(" ").append(vertex);
                        ++unvisitedCount;
                    }
                }

                System.out.println(unvisitedCount + sb.toString());
            }


        }
    }

    private static boolean[] dfs(List<List<Integer>> adjacencyList, int startNode){
        Deque<Integer> stack = new ArrayDeque<>();
        boolean[] visited = new boolean[adjacencyList.size()];

        stack.push(startNode);

        while (!stack.isEmpty()){
            for (Integer neighbour : adjacencyList.get(stack.pop() - 1)) {
                if (!visited[neighbour - 1]) {
                    visited[neighbour - 1] = true;
                    stack.push(neighbour);
                }
            }
        }

        return visited;
    }
}
