package cz.zcu.fav.kiv.asp.prochazm;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

class MergeSort {

    // Mergesort pomoci rozdel a panuj.
    //
    // Pouzijte rekurzi a pomocna pole (List/ArrayList), muzete klidne
    // pouzit i metodu List.subList(fromInclusive, toExclusive) a treba i Iterator.
    //
    // Vstup: Neusporadane pole cisel, read-only
    // Vystup: Pole cisel usporadane do neklesajici posloupnosti
    static List<Integer> mergesort(List<Integer> a) {
        int[] in = a.stream().mapToInt(Integer::intValue).toArray();
        int[] out = Arrays.copyOf(in, in.length);

        split(in, out, 0, a.size());

        return Arrays.stream(out).boxed().collect(Collectors.toList());
    }

    static void split(int[] tmp, int[] result, int from, int to){
        if (to - from < 2){
            return;
        }

        int half = (from + to) / 2;

        split(result, tmp, from, half);
        split(result, tmp, half, to);
        merge(tmp, result, from, half, to);
    }

    static void merge(int[] a1, int[] a2, int from, int half, int to){
        int i = from;
        int j = half;

        for (int k = from; k < to; ++k){
            a2[k] = i < half && (j >= to || a1[i] <= a1[j]) ? a1[i++] : a1[j++];
        }
    }

    public static void main(String[] args) throws IOException {
        // Rychlejsimu zpracovani vstupu a vystupu
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter pw = new PrintWriter(System.out);

        String line;
        while ((line = br.readLine()) != null) { // Cte radky az do EOF

            // Nacte pole celych cisel
            List<Integer> a = new ArrayList<>();
            for (String x: line.trim().split("\\s+")) {
                a.add(Integer.valueOf(x));
            }

            // Prazdny radek => konec (jen kvuli uzivatelskemu testovani)
            if (a.isEmpty()) {
                break;
            }

            // Usporada cisla vzestupne
            Iterator<Integer> it = mergesort(Collections.unmodifiableList(a)).iterator();

            // Na vystup pujdou usporadana cisla oddelena mezerami
            StringBuilder sb = new StringBuilder();
            sb.append(it.next());
            while (it.hasNext()) {
                sb.append(' ').append(it.next());
            }

            // Vypise radek usporadanych cisel
            pw.println(sb);

            pw.flush(); // jen kvuli uzivatelskemu testovani
        }

        pw.close(); // Musime zavrit vystup, jinak nemusi byt kompletni!
    }
}