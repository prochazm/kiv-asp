package cz.zcu.fav.kiv.asp.prochazm;

import java.io.*;
import java.util.*;

import static java.lang.Integer.*;

public class Slepice {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String[] split = br.readLine().split(" ");
        final int portalCount = parseInt(split[0]);
        final int chickenCount = parseInt(split[1]);

        List<Section> events = new ArrayList<>();

        for (int i = 0; i < portalCount; ++i) {
            split = br.readLine().split(" ");
            final int y = parseInt(split[1]);
            final int rad = parseInt(split[2]);

            events.add(new Section(parseInt(split[0]), y - rad,y + rad, false));
        }

        for (int i = 0; i < chickenCount; ++i){
            split = br.readLine().split(" ");
            events.add(new Section(parseInt(split[0]), parseInt(split[1]), 0, true));
        }

        events.sort(Section::compareTo);

        NavigableSet<Section> sweepLine = new TreeSet<>(Section::compareByY);
        SortedMap<Integer, Integer> results = new TreeMap<>();

        for (Section section : events) {
            if (section.isHorizontal){
                sweepLine.add(section);
                continue;
            }

            Iterator<Section> rangeIterator = sweepLine
                    .subSet(section, true, new Section(0,section.y2 + 1, 0, true), true)
                    .iterator();

            while (rangeIterator.hasNext()){
                results.put(rangeIterator.next().id, section.id);
                rangeIterator.remove();
            }
        }

        sweepLine.forEach(s -> results.put(s.id, 0));
        results.values().forEach(System.out::println);
    }

    static class Section implements Comparable<Section> {
        private static int lastId;

        final int id;
        final int x1;
        final int y1;
        final int y2;
        final boolean isHorizontal;

        Section(int x1, int y1, int y2, boolean isHorizontal) {
            this.x1 = x1;
            this.y1 = y1;
            this.y2 = y2;
            this.isHorizontal = isHorizontal;
            this.id = ++lastId;
        }

        @Override
        public int compareTo(Section section) {
            int xDiff = this.x1 - section.x1;
            return xDiff == 0 && this.isHorizontal ? -1 : xDiff;
        }

        int compareByY(Section section) {
            return this.y1 > section.y1 ? 1 : -1;
        }
    }
}