package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

import static java.lang.Integer.*;

public class Movo {

    public static void main(String[] args) throws IOException {
        final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        final int count = parseInt(br.readLine());
        final List<Node> nodes = new ArrayList<>(count);

        String[] line;

        for (int i = 0; i < count; i++) {
            line = br.readLine().split(" ");
            nodes.add(new Node(parseInt(line[0]), parseInt(line[1]), parseInt(line[2]), parseInt(line[3])));
        }

        nodes.sort(Node::compareTo);


        Node n;
        for (n = nodes.get(0); n.next != null && !n.next.visited; n = n.next){
            n.visited = true;
        }

        System.out.println(n.next == null
                ? String.format("%d %d", n.x1, n.y1)
                : "Loop"
        );
    }

    private static class Node implements Comparable<Node> {
        Node next;

        final int x1;
        final int y1;
        final int x2;
        final int y2;

        boolean visited = false;

        Node(int x, int y, int dx, int dy){
            this.x1 = x;
            this.y1 = y;
            this.x2 = dx == 0 ? x : dx == 1 ? MAX_VALUE : MIN_VALUE;
            this.y2 = dy == 0 ? y : dy == 1 ? MAX_VALUE : MIN_VALUE;
        }

        private boolean pointsTo(Node n){

        }

        @Override
        public int compareTo(Node node) {
            if (this.x1 != node.x1) {
                return this.x1 - node.x1;
            }
            return this.y1 - node.y1;
        }
    }
}
