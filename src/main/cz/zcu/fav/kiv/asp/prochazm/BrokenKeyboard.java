package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Deque;
import java.util.LinkedList;

public class BrokenKeyboard {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line;

        while ((line = br.readLine()) != null){
            Deque<String> output = new LinkedList<>();
            StringBuilder tmp = new StringBuilder();
            boolean append = true;

            for (char c : line.toCharArray()) {
                if (c == '[' || c == ']'){
                    join(append, output, tmp);
                    tmp.setLength(0);
                    append = c == '[';
                } else {
                    tmp.append(c);
                }
            }

            join(append, output, tmp);
            output.forEach(System.out::print);
            System.out.println();
        }
    }

    private static void join(boolean append, Deque<String> deque, StringBuilder sb){
        if (append){
            deque.addFirst(sb.toString());
            return;
        }
        deque.addLast(sb.toString());
    }
}
