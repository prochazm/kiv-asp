package cz.zcu.fav.kiv.asp.prochazm;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ThrowingCardsAway {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int deckSize;

        while ((deckSize = Integer.parseInt(br.readLine())) != 0){
            Deck deck = new Deck(deckSize);
            StringBuilder sb = new StringBuilder("Discarded cards: ");

            while(deck.size() > 1){
                sb.append(deck.poll());  // discard
                deck.offer(deck.poll()); // shuffle back
                sb.append(", ");
            }

            if (sb.length() > 17) {
                sb.delete(sb.length() - 2, sb.length());
            } else {
                sb.delete(sb.length() - 1, sb.length());
            }

            System.out.println(sb.toString());
            System.out.println("Remaining card: " + (deck.size() > 0 ? deck.poll() : ""));
        }
    }

    private static class Deck {
        private final int[] items;
        private int head;
        private int tail;

        Deck(int length){
            this.items = new int[length * 2];
            for(int i = 1; i <= length; ++i){
                this.items[i - 1] = i;
            }
            this.head = 0;
            this.tail = length;
        }

        int poll(){
            return this.items[this.head++];
        }

        void offer(int i){
            this.items[this.tail++] = i;
        }

        int size(){
            return this.tail - this.head;
        }
    }
}
