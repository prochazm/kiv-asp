package cz.zcu.fav.kiv.asp.prochazm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class SecurePins {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int count = Integer.parseInt(br.readLine());
        char[] line;

        for (int i = 0; i < count; ++i){
            line = br.readLine().toCharArray();

            if (hasMultiOccurrence(line)
                    || hasInsecureSequence(line, 1)
                    || hasInsecureSequence(line, -1)){
                System.out.println("WEAK");
                continue;
            }

            System.out.println("ACCEPTABLE");
        }
    }

    private static boolean hasMultiOccurrence(char[] input){
        int[] occurrences = new int[10];
        Arrays.fill(occurrences, 0);

        for (char c : input) {
            ++occurrences[c - '0'];
            if (occurrences[c - '0'] == 3){
                return true;
            }
        }

        return false;
    }

    private static boolean hasInsecureSequence(char[] input, int direction){
        char last = 0;
        int sequenceRank = 0;

        for (char c : input) {
            if (last + direction == c){
                ++sequenceRank;
                if (sequenceRank == 2){
                    return true;
                }
            } else {
                sequenceRank = 0;
            }
            last = c;
        }

        return false;
    }
}
